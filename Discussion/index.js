


// array manipulation push

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);

// array manipulation pop
/*
	- removes the last element in an array and returns the removed element.
	- Syntax 
		arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);

// unshift()
/*
	- Adds one or more elements at the beginning of an array.
	Syntax:

		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

// shift()
/*
	- Removes an element at the beginning of an array and returns the removed element
	- Syntax
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice()
/*
	- Simultaneously removes elements from a specified index number and adds element
	- Syntax
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

const try1 = fruits.splice(1, 3, 'Lime', 'Cherry', 'Lemon');
console.log(try1);
console.log('Mutated array from splice method:');
console.log(fruits);

// sort()
/*
	- Rearrange the array elements in alphanumeric order. 
	- Syntax:
		arrayName.sort();
*/

const randomThings = ["Cat", "boy", "apps", "zoo"]

randomThings.sort();
console.log('Mutated array from sort method:');
console.log(randomThings);

// reverse()

/*
	- Reverse the order of array elements
	- Syntax
		arrayName.reverse();
*/

randomThings.reverse();
console.log('Mutated array from reverse method:');
console.log(randomThings);

// Non-Mutator Methods
/*
	- Non-Mutator methods are functions that do not modify or change an array after they're created.
	- These method do not manipulate original array performing various tasks such as returning elements from an array and combining array and printing the output.
*/

// indexOf()

/*
	- returns the index number of the first matching element found in an array.
	- if no match was found, the result will be -1
	- the search process will be done from first element proceeding to the last element.

	- Syntax:
		arrayName.indexOf(searchValue)
*/
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE']
console.log('Countries:')
console.log(countries);
let firstIndex = countries.indexOf('CAN');
console.log('Result of indexOf method: ' + firstIndex);


// slice()
/*
	-Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

let slicedArrayA = countries.slice(4);
console.log('Result from slice method:');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method:');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-5);
console.log('Result from slice method:');
console.log(slicedArrayC);

// forEach();

/*
	- Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- arrayName - plural for good practice
	-parameter - singular
	- Note: it must have function.

	- Syntax:
		arrayName.forEach(function(indivElement){
			statement;
		})
*/

countries.forEach(function(country) {
	console.log(country);
});

// includes() 

/*
	includes() 

	- includes() method checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable.
		- return true if the argument is found in the array.
		- return false if it is not.

		- Syntax:
			arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log(productFound1);//returns true

let productFound2 = products.includes("Headset");

console.log(productFound2);//returns false

